# Neural Graph Consensus - AAAI 2021 version

Repository for the AAAI 2021 paper.

Project page: https://sites.google.com/site/aerialimageunderstanding/semi-supervised-learning-of-multiple-scene-interpretations-by-neural-graph?authuser=0

## Data

The repository assumes the data comes from the CARLA simulator in the format provided by the project page.

```
> ls /path/to/carla_ngc_data
test_set_256_npz  train_set_256_npz_iter0  train_set_256_npz_iter1  train_set_256_npz_iter2  validation_set_256_npz
```

## Defining the graph
The yaml cfg file is [here](cfgs/carla/graph_paper.yaml).

## Training an edge

Given any valid edge from the graph cfg file (i.e. rgb_semanticSegmentation), use:

```
python3 main.py retrain --ngcDir /path/to/ngcdir --cfgPath cfgs/carla/graph_paper.yaml --datasetPath /path/to/carla_ngc_data/train_set_npz_iterX --validationDatasetPath /path/to/carla_ngc_data/validation_set_256_npz --currentEdge XXX --trainCfgPath cfgs/train/train_adam_1.yaml
```

## Testing metrics for the graph

Using the pretrained weights provided by the project page, set `--ngcDir` to the directory where the weights are downloaded.

```
> ls /path/to/ngcDir
iteration0  iteration1  iteration2  iteration3  paper_results.tar.gz

> ls /path/to/ngcDir/iteration0/
depth_semanticSegmentation  rgb_cameranormal  rgb_depth  rgb_halftone  rgb_normal  rgb_semanticSegmentation  rgb_wireframeRegression  semanticSegmentation_depth ... (all edges according to graph cfg)

```

Then, use:
```
python3 main.py retrain --ngcDir /path/to/ngcdir/iterXXX --cfgPath cfgs/carla/graph_paper.yaml --datasetPath /path/to/carla_ngc_data/test_set_npz 
```

The result will yield a list of metrics for single links as well as graph ensembles.

## Exporting pseudolabels

Using the pretrained weights for iteration XXX-1, we can export the graph ensemble pseudolabels for iteration XXX on any given semi supervised set.

Use:
```
python3 main.py retrain --ngcDir /path/to/ngcdir/iterXXX-1 --cfgPath cfgs/carla/graph_paper.yaml --datasetPath /path/to/carla_ngc_data/semisupervised_XXX --pseudolabelsDir=/path/to/ngcdir/iterXXX
```

This will result in a set of pseudolabels in the same format as the original dataset. The `semisupervised_XXX` dataset should provide only `RGB` inputs as these is the only ground truth information required for iterations above first (where real GT is used, instead of pseudolabels).