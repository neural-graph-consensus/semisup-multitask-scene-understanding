from ngclib.nodes import CameraNormal as _CameraNormal
from nwmodule.graph import MapNode
from ..Map2Map import EncoderMap2Map, DecoderMap2Map

class CameraNormal(_CameraNormal):
    def getEncoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.numDims)

    def getDecoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.numDims)

    def aggregate(self):
        pass