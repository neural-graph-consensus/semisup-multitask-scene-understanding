from ngclib.nodes import RGB as _RGB
from nwmodule.graph import MapNode
from ..Map2Map import EncoderMap2Map, DecoderMap2Map

class WireframeRegression(_RGB):
    def __init__(self):
        MapNode.__init__(self, name="WireframeRegression", groundTruthKey="wireframe_regression", numDims=3)

    def getEncoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.numDims)

    def getDecoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.numDims)

    def aggregate(self):
        pass
