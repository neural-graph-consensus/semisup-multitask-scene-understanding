from .rgb import RGB
from .depth import Depth
from .semantic import Semantic
from .wireframe_regression import WireframeRegression
from .camera_normal import CameraNormal
from .normal import Normal
from .halftone import Halftone