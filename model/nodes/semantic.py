import torch as tr
from ngclib.nodes import Semantic as _Semantic
from nwmodule.graph import MapNode, Message
from ..Map2Map import EncoderMap2Map, DecoderMap2Map

class Semantic(_Semantic):
	def __init__(self, semanticClasses, useGlobalMetrics:bool=False):
		super().__init__(semanticClasses, useGlobalMetrics)
		assert len(self.semanticClasses) == 13, self.semanticClasses
		self.semanticColors = [(0, 0, 0), (70, 70, 70), (153, 153, 190), (160, 170, 250), \
			(60, 20, 220), (153, 153, 153), (50, 234, 157), (128, 64, 128), (232, 35, 244), \
			(35, 142, 107), (142, 0, 0), (156, 102, 102), (0, 220, 220)]
		assert len(self.semanticClasses) == len(self.semanticColors)

	def getEncoder(self, outputNode):
		assert isinstance(outputNode, MapNode)
		return EncoderMap2Map(dIn=self.numDims)

	def getDecoder(self, inputNode):
		assert isinstance(inputNode, MapNode)
		return DecoderMap2Map(dOut=self.numDims)

	def aggregate(self):
		messages = self.getMessages()
		newPath = ["Reduce"]
		newInput = tr.stack([x.output.detach() for x in messages], dim=0)
		newOutput = sum(newInput) / len(newInput)
		self.clearMessages()
		self.addMessage(Message(newPath, newInput, newOutput))
