import numpy as np
from typing import get_type_hints

from nwmodule.utilities import device, npGetData
from nwdata.datasets.carla.plot_utils import toImageFuncs
from media_processing_lib.image import tryWriteImage

from ngclib.models import *
from ngclib.nodes import *

from .nodes import *

def graphPlotFn(x, y, t, model):
	cnt = 0
	MB = t[tuple(t.keys())[0]].shape[0]

	for i in range(MB):
		for edge in model.edges:
			edgeID = str(edge)
			A, B = edge.inputNode, edge.outputNode
			strA, strB = str(A).split(" ")[0], str(B).split(" ")[0]
			assert edgeID in y and len(y[edgeID]) == 1
			edgeResult = npGetData(y[edgeID])[0, i]

			# TODO -- use messages.
			if not A.groundTruthKey in t:
				print("%s (gt key=%s) not in data." % (A, A.groundTruthKey))
				edgeInput = edgeResult * 0
			else:
				edgeInput = t[A.groundTruthKey][i]

			if not B.groundTruthKey in t:
				print("%s (gt key=%s) not in data." % (B, B.groundTruthKey))
				edgeGT = edgeResult * 0
			else:
				edgeGT = t[B.groundTruthKey][i]

			edgeInputImage = toImageFuncs[strA](edgeInput)
			edgeResultImage = toImageFuncs[strB](edgeResult)
			edgeGTImage = toImageFuncs[strB](edgeGT)

			stack = np.concatenate([edgeInputImage, edgeResultImage, edgeGTImage], axis=1)
			j = 0 # TODO: See that we can have multiple inputs to the node of the edge
			tryWriteImage(stack, "%d_edge%s_%d.png" % (cnt, edgeID, j + 1))
		cnt += 1

def getModel(args):
	modelType = args.cfg["graphConfig"]
	assert modelType == "NGC"
	nodeTypes = {Type:globals()[Type] for Type in args.cfg["nodeTypes"]}
	model = NGC(cfg=args.cfg, nodeTypes=nodeTypes, singleLinksPath=args.ngcDir)
	model.setTrainableWeights(False)
	model = model.to(device)
	return model